USE SuperheroesDb;

ALTER TABLE Assistant
ADD HeroId int FOREIGN KEY REFERENCES Superhero(Id);