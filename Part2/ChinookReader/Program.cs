﻿public static class Program
{

	public static void Main()
	{
		SqlCustomerClientHelper sql = new();

		// 2.1
		Console.WriteLine("2.1 start");
		var c = sql.ReadAllCustomers();
		foreach (var item in c)
		{
			Logger.LogCustomer(item);
		}
		Console.WriteLine("2.1 end");

		// 2.2
		Console.WriteLine("2.2 start");
		var t = sql.ReadCustomer(10);
		Logger.LogCustomer(t);
		Console.WriteLine("2.2 end");

		// 2.3
		Console.WriteLine("2.3 start");
		t.Country = "Antartica";
		sql.UpdateCustomer(t);
		t = sql.ReadCustomer(10);
		Logger.LogCustomer(t);
		Console.WriteLine("2.3 end");

		// 2.4
		Console.WriteLine("2.4 start");
		var d = sql.ReadCustomer("František");
		Logger.LogCustomer(d);
		Console.WriteLine("2.4 end");

		// 2.5
		Console.WriteLine("2.5 start");
		Customer tom = new(0, "Tom", "Tomington", null, null, null, "tt@tt.com");
		sql.CreateCustomer(tom);
		tom = sql.ReadCustomer("Tom");
		Logger.LogCustomer(tom);
		Console.WriteLine("2.5 end");

		// 2.6
		Console.WriteLine("2.6 start");
		var l = sql.ReadAllCustomers(5, 10);
		foreach (var item in l)
		{
			Logger.LogCustomer(item);
		}
		Console.WriteLine("2.6 end");

		// 2.7
		Console.WriteLine("2.7 start");
		var q = sql.NumberOfCustomersInEachCountry();
		foreach (var item in q)
		{
			Logger.LogCountryCounter(item);
		}
		Console.WriteLine("2.7 end");

		// 2.8
		Console.WriteLine("2.8 start");
		var o = sql.GetCustomersSortedBySpending();
		foreach (var item in o)
		{
			Logger.LogCustomerSpender(item);
		}
		Console.WriteLine("2.8 end");

		// 2.9
		Console.WriteLine("2.9 start");
		var g = sql.GetCustomerFavouriteGenre(t.Id);
		foreach (var item in g)
		{
			Logger.LogCustomerGenre(item);
		}
		Console.WriteLine("2.9 end");


	}
}
